#include <regex>
#include <atomic>
#include <chrono>
#include <cstdio>
#include <future>
#include <string>
#include <memory>
#include <thread>
#include <cstdlib>
#include <utility>
#include <stdexcept>

extern "C" {
#include <unistd.h>
#include <linux/input.h>
}

#include <boost/interprocess/ipc/message_queue.hpp>

using std::chrono::milliseconds;
using std::this_thread::sleep_for;
using boost::interprocess::open_only;
using boost::interprocess::message_queue;

void print_usage(FILE *stream, const char *program) {
    // clang-format off
    fprintf(stream,
            "uswitch - redirect stdin to a muxer if logged user matches\n"
            "\n"
            "usage: %s [-h] [-r rate] [-u username] [-o muxer]\n"
            "\n"
            "options:\n"
            "    -h         show this message and exit\n"
            "    -r rate    rate in milliseconds to check name of current logged user (default: 2000)\n"
            "    -u user    regex to match username logged in the active TTY\n"
            "    -o muxer   name of muxer to switch to\n",
            program);
    // clang-format on
}

std::string get_username() {
    FILE *p =
        popen(R"(who | grep -Em 1 "\S\s+`cat /sys/class/tty/tty0/active`\s+\S" | awk '{printf "%s", $1;}')", "r");
    if (!p)
        return "";

    char username[100] = {0}, *result;

    result = std::fgets(username, sizeof(username), p);
    pclose(p);
    if (!result)
        return "";

    return username;
}

unsigned long rate = 2000;
std::atomic<bool> finish{false};
std::atomic<size_t> current_muxer{0};

int main(int argc, char *argv[]) try {
    std::vector<std::regex> user_filters;
    std::vector<std::vector<std::unique_ptr<message_queue>>> muxers;

    muxers.emplace_back();

    for (int opt, last_opt = 0; (opt = getopt(argc, argv, "hr:u:o:")) != -1;) {
        switch (opt) {
            case 'h':
                return print_usage(stdout, argv[0]), EXIT_SUCCESS;
            case 'r':
                if (last_opt)
                    break;
                rate     = std::stoul(optarg);
                last_opt = 'r';
                continue;
            case 'u':
                if (last_opt == 'u')
                    break;
                user_filters.emplace_back(optarg, std::regex::optimize);
                last_opt = 'u';
                continue;
            case 'o':
                if (last_opt == 'u')
                    muxers.emplace_back();
                muxers.back().emplace_back(
                    new message_queue(open_only, optarg));
                last_opt = 'o';
                continue;
        }

        return print_usage(stderr, argv[0]), EXIT_FAILURE;
    }

    muxers.emplace_back();

    auto current_user_loop = std::async(
        std::launch::async,
        [](const std::vector<std::regex> &user_filters) {
            try {
                while (!finish) {
                    size_t current       = 0;
                    std::string username = get_username();
                    if (!username.empty()) {
                        size_t muxer = 1;
                        for (const auto &user_filter : user_filters) {
                            if (std::regex_match(username, user_filter)) {
                                current = muxer;
                                break;
                            }
                            ++muxer;
                        }
                    }
                    current_muxer = current;
                    sleep_for(milliseconds(rate));
                }
            } catch (...) {
                finish = true;
                throw;
            }
        },
        std::move(user_filters));

    struct defer {
        ~defer() { finish = true; }
    } defer;

    std::setbuf(stdin, nullptr);

    input_event input;
    while (!finish)
        if (std::fread(&input, sizeof input, 1, stdin) == 1) {
            size_t current = current_muxer;
            for (auto &muxer : muxers[current])
                if (!muxer->try_send(&input, sizeof input, 0))
                    throw std::runtime_error("outgoing muxer is full, exiting");
        } else if (std::ferror(stdin))
            throw std::runtime_error("error reading input event from stdin");
        else if (std::feof(stdin))
            break;
} catch (const std::exception &e) {
    return std::fprintf(stderr,
                        R"(an exception occurred: "%s")"
                        "\n",
                        e.what()),
           EXIT_FAILURE;
}
