# uswitch

`uswitch` is an additional tool for [_Interception Tools_][interception-tools]
that works similarly to the [`mux`] tool in switch mode, but that **switch**
_muxers_ based on current user logged from active TTY (instead of muxer activity).

The purpose is to have user specific shortcuts, chording, etc.

## Dependencies

- [Interception Tools][interception-tools]

## Build dependencies

- [CMake]
- [Boost.Interprocess][interprocess]

## Building

```
$ git clone https://gitlab.com/interception/linux/plugins/uswitch.git
$ cd uswitch
$ cmake -B build -DCMAKE_BUILD_TYPE=Release
$ cmake --build build
```

## Execution

```
uswitch - redirect stdin to a muxer if logged user matches

usage: uswitch [-h] [-r rate] [-u username] [-o muxer]

options:
    -h         show this message and exit
    -r rate    rate in milliseconds to check name of current logged user (default: 2000)
    -u user    regex to match username logged in the active TTY
    -o muxer   name of muxer to switch to
```

`uswitch` is an [_Interception Tools_][interception-tools] tool. An example
application:

```yaml
- CMD: mux -c keyboard -c caps2esc
- JOB:
    - mux -i caps2esc | caps2esc | mux -o keyboard
    - mux -i keyboard | uinput -c /etc/interception/keyboard.yaml
- JOB: intercept -g $DEVNODE | uswitch -o keyboard -u francisco -o caps2esc
  DEVICE:
    LINK: /dev/input/by-path/platform-i8042-serio-0-event-kbd
```

Which will grab the i8042 keyboard (`intercept -g $DEVNODE`) once it's detected
(`LINK: /dev/input/by-path/platform-i8042-serio-0-event-kbd`), and redirect its
events to _muxer_ `keyboard` by default (`uswitch -o keyboard …`) or to _muxer_
`caps2esc`, if user logged into the active TTY is “francisco”
(`uswitch … -u francisco -o caps2esc`).

Endpoint at `keyboard` _muxer_ is readily consumed by the virtual keyboard
clone (`mux -i keyboard | uinput -c /etc/interception/keyboard.yaml`), while at
pipeline `caps2esc`, there's a detour for applying `caps2esc` before it goes
for consumption (`mux -i caps2esc | caps2esc | mux -o keyboard`). So `caps2esc`
is either active or not depending upon whether user is “francisco”.

The clone of the i8042 keyboard is created from [previously
stored][device-links] configuration snapshot (`sudo uinput -p -d
/dev/input/by-path/platform-i8042-serio-0-event-kbd | sudo tee
/etc/interception/keyboard.yaml`).

The `uswitch` tool accepts an output (`-o X`), or multiple (`-o Y -o Z`), that
will be redirected to (with duplicated redirection if multiple), when current
user matches the `-u` flag passed before it (`-u francisco -o X`). For `-o`
passed first without `-u` coming before, it's set as default output for non
matched user, which is generally advisable to have. To sum it up, `-u` can be
passed multiple times, with many `-o`s following each `-u`, to provide specific
outputs for each match.

For more information about the [_Interception Tools_][interception-tools],
check the project's website.

## Installation

I'm maintaining an Archlinux package on AUR:

- <https://aur.archlinux.org/packages/interception-uswitch>

## License

<a href="https://gitlab.com/interception/linux/plugins/uswitch/blob/master/LICENSE.md">
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/License_icon-mit-2.svg/120px-License_icon-mit-2.svg.png" alt="MIT">
</a>

Copyright © 2021 Francisco Lopes da Silva

[interception-tools]: https://gitlab.com/interception/linux/tools
[`mux`]: https://gitlab.com/interception/linux/tools#mux
[cmake]: https://cmake.org
[interprocess]: https://www.boost.org/doc/libs/release/libs/interprocess
[device-links]: https://gitlab.com/interception/linux/tools#device-links
[udevmon]: https://gitlab.com/interception/linux/tools/-/blob/master/udevmon.service
